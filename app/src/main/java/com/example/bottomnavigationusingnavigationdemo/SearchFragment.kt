package com.example.bottomnavigationusingnavigationdemo


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * A simple [Fragment] subclass.
 */
class SearchFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_search, container, false)

        val searchAdapter = SearchAdapter(this, mutableListOf())
        val pager = view.findViewById<ViewPager2>(R.id.pager)
        pager.adapter = searchAdapter

        val tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)
        var tabName = ""
        TabLayoutMediator(tabLayout, pager) { tab, position ->
            when(position) {
                0 -> tabName = "Best Match"
                1 -> tabName = "New"
                2 -> tabName = "Nearby"
            }
            tab.text = tabName
        }.attach()

        MyLogger.log(this.javaClass.simpleName, "Fragment oncreateview called")

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        MyLogger.log(this.javaClass.simpleName, "Fragment on attach called")

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MyLogger.log(this.javaClass.simpleName, "Fragment on create called")

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        MyLogger.log(this.javaClass.simpleName, "Fragment onactivitycreated called")

    }

    override fun onStart() {
        super.onStart()
        MyLogger.log(this.javaClass.simpleName, "Fragment onstart called")
    }

    override fun onPause() {
        super.onPause()
        MyLogger.log(this.javaClass.simpleName, "Fragment onpause called")
    }

    override fun onResume() {
        super.onResume()
        MyLogger.log(this.javaClass.simpleName, "Fragment onresume called")
    }

    override fun onStop() {
        super.onStop()
        MyLogger.log(this.javaClass.simpleName, "Fragment onstop called")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MyLogger.log(this.javaClass.simpleName, "Fragment ondestroyview called")
    }

    override fun onDestroy() {
        super.onDestroy()
        MyLogger.log(this.javaClass.simpleName, "Fragment ondestroy called")
    }

    override fun onDetach() {
        super.onDetach()
        MyLogger.log(this.javaClass.simpleName, "Fragment ondetach called")
    }
}

