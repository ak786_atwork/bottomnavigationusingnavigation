package com.example.bottomnavigationusingnavigationdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.arch.core.util.Function
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

class TestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        val liveData = MutableLiveData<String>()

        val liveData1 = Transformations.map(liveData) { it.length}

        val liveData2 = Transformations.switchMap(liveData, {it -> getLiveData(it)})
    }

    private fun getLiveData(value: String?): MutableLiveData<Int>? {
        val lenLiveData = MutableLiveData<Int>()
        lenLiveData.value = value?.length
        return lenLiveData
    }
}
