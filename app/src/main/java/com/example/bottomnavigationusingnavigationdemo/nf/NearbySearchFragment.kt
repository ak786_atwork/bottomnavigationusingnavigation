package com.example.bottomnavigationusingnavigationdemo.nf


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.bottomnavigationusingnavigationdemo.MyLogger

import com.example.bottomnavigationusingnavigationdemo.R

/**
 * A simple [Fragment] subclass.
 */
class NearbySearchFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        MyLogger.log(this.javaClass.simpleName, "oncreateview is called")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_nearby_search, container, false)
    }

  companion object{
      // newInstance constructor for creating fragment with arguments
      fun newInstance(page: Int, title: String): NearbySearchFragment {
          val fragment = NearbySearchFragment()
          val args = Bundle()
          args.putInt("someInt", page)
          args.putString("someTitle", title)
          fragment.arguments = args
          return fragment
      }
  }

    override fun onAttach(context: Context) {
        MyLogger.log(this.javaClass.simpleName, "Fragment on attach called")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        MyLogger.log(this.javaClass.simpleName, "Fragment on create called")

        super.onCreate(savedInstanceState)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        MyLogger.log(this.javaClass.simpleName, "Fragment onactivitycreated called")
        super.onActivityCreated(savedInstanceState)

    }

    override fun onStart() {
        MyLogger.log(this.javaClass.simpleName, "Fragment onstart called")
        super.onStart()

    }

    override fun onPause() {
        MyLogger.log(this.javaClass.simpleName, "Fragment onpause called")
        super.onPause()

    }

    override fun onResume() {
        MyLogger.log(this.javaClass.simpleName, "Fragment onresume called")
        super.onResume()

    }

    override fun onStop() {
        MyLogger.log(this.javaClass.simpleName, "Fragment onstop called")
        super.onStop()

    }

    override fun onDestroyView() {
        MyLogger.log(this.javaClass.simpleName, "Fragment ondestroyview called")
        super.onDestroyView()

    }

    override fun onDestroy() {
        MyLogger.log(this.javaClass.simpleName, "Fragment ondestroy called")
        super.onDestroy()

    }

    override fun onDetach() {
        MyLogger.log(this.javaClass.simpleName, "Fragment ondetach called")
        super.onDetach()

    }

}
