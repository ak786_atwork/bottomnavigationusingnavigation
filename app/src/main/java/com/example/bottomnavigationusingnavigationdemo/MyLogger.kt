package com.example.bottomnavigationusingnavigationdemo

import android.util.Log

class MyLogger {
    companion object {
        fun log(className: String, info: String) {
            Log.d("Debug $className", info)
        }
    }
}