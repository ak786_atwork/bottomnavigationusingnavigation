package com.example.bottomnavigationusingnavigationdemo

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.bottomnavigationusingnavigationdemo.nf.BestMatchSearchFragment
import com.example.bottomnavigationusingnavigationdemo.nf.NearbySearchFragment
import com.example.bottomnavigationusingnavigationdemo.nf.NewSearchFragment

class SearchAdapter(fragment: Fragment, private val list: MutableList<Fragment>) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        if (position < list.size)
            list[position]
        when(position) {
            0 -> list.add(0,BestMatchSearchFragment.newInstance(0,"best"))
            1 -> list.add(1, NewSearchFragment.newInstance(1,"new"))
            2 -> list.add(2, NearbySearchFragment.newInstance(2,"nearby"))
        }
        return list[position]
    }
}