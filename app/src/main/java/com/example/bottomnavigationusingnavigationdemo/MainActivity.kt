package com.example.bottomnavigationusingnavigationdemo

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MyLogger.log(this.javaClass.simpleName, "activity oncreate is called")
        setUpBottomNavigation()

    }

    private fun setUpBottomNavigation() {
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupWithNavController(bottom_nav, navController)
    }




    override fun onStart() {
        MyLogger.log(this.javaClass.simpleName, "activity onstart called")
        super.onStart()

    }

    override fun onPause() {
        MyLogger.log(this.javaClass.simpleName, "activity onpause called")
        super.onPause()

    }

    override fun onResume() {
        MyLogger.log(this.javaClass.simpleName, "activity onresume called")
        super.onResume()

    }

    override fun onStop() {
        MyLogger.log(this.javaClass.simpleName, "activity onstop called")
        super.onStop()

    }


    override fun onDestroy() {
        MyLogger.log(this.javaClass.simpleName, "activity ondestroy called")
        super.onDestroy()

    }

}
